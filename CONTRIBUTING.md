# Contribuer

## Fichiers

### "Paramètres" des fichiers textes

Un fichier texte est un fichier lisible par un éditeur de texte (comme GNU nano).
Par exemple, des fichiers contenant du Markdown, TeX, HTML, SVG et/ou du code source en C, C++, Go, Java, Scala, Python sont considérés comme des fichiers textes, mais des fichiers en OpenDocument et PDF ne le sont pas.

Tous les fichiers textes doivent être en [UTF-8](https://fr.wikipedia.org/wiki/UTF-8) (sans [BOM](https://fr.wikipedia.org/wiki/Indicateur_d%27ordre_des_octets)) (genéralement par défaut UTF-8 sur GNU/Linux et [ISO 8859-15](https://fr.wikipedia.org/wiki/ISO/CEI_8859-15) sur MS Windows) et [fin de ligne](https://fr.wikipedia.org/wiki/Fin_de_ligne) LF (parfois appelé fin de ligne UNIX) (Windows utilise généralement CRLF).
Malheuresement, par défaut certains éditeurs de fichiers textes enregistrent avec le même encodage et la même fin de ligne que l'OS.

Une ligne vide doit être présente à la fin de chaque fichier texte.
Cela est utile pour la lisibilité avec [`cat file`](https://fr.wikipedia.org/wiki/Cat_%28Unix%29).

#### Configuration des "paramètres" des fichiers textes

- Si votre éditeur gère [EditorConfig](http://editorconfig.org/), vous n'avez rien à faire, profitez !
- [Geany](http://geany.org/) : Pour créer de nouveau fichiers en UTF-8 et LF, allez dans "Editer" > "Préférences" > "Fichiers" (onglet), ensuite choisissez "Unix (LF)" pour les fins de lignes et "UTF-8" comme encodage par défaut.

### Code source

### Fichiers textes formatés

Si vous voulez un document texte un peu structuré, vous devriez considérer [Markdown](https://fr.wikipedia.org/wiki/Markdown).
Par exemple, ce document utilise la syntaxe Markdown.

Pour un texte long ou nécessitant plus de possibilités que Markdown ou une présentation, il y a [LaTeX](http://latex-project.org/).
Par exemple, LaTeX est utilisé pour le cours et la présentation.

#### Markdown

Un élément de liste doit être fait avec "-" (charactère moins) pour éviter la confusion avec "*" qui peut être utilisé pour [l'emphase](https://fr.wikipedia.org/wiki/Emphase_%28typographie%29).
L'extension de fichiers Markdown doit être "md" ou "markdown", l'extension "mkd" n'est pas communément reconnu comme Markdown donc elle ne doit pas être utilisée.

#### LaTeX

##### Apprendre

Vous pouvez apprendre par l'exemple en lisant le code LaTeX de ce projet.
Modifier des éléments et voir le résultat en PDF vous aidera à comprendre.

[Framabook propose un livre sur LaTeX en version numérique gratuite et papier](http://framabook.org/tout-sur-latex/).

Pour rédiger des documents, [OpenClassRooms propose un tutoriel lui aussi numérique et papier](https://openclassrooms.com/courses/redigez-des-documents-de-qualite-avec-latex).
Sur ce même site web, il y a aussi [un cours pour créer un diaporama avec LaTeX et le package beamer](https://openclassrooms.com/courses/creez-vos-diaporamas-en-latex-avec-beamer).

## Git

### Installation

#### Debian

Le texte ci-dessous devrait aussi marcher sur les dérivés de [Debian](https://www.debian.org/), comme [Trisquel](https://trisquel.info/), [gNewSense](http://www.gnewsense.org/), Ubuntu et Mint.

Vous devez installer le paquet git et ses dépendances.
Vous devez avoir les droits [SuperUser/root](https://fr.wikipedia.org/wiki/Utilisateur_root), que vous pouvez avoir avec `su` ou `sudo command`.
Vous pouvez utiliser `apt-get install`, `aptitude install` (si [aptitude](https://wiki.debian.org/Aptitude) est installé) ou une interface graphique comme [Synaptic](https://wiki.debian.org/Synaptic) (s'il est installé).

#### Fedora, CentOS et RHEL

Vous devez installer le paquet git et ses dépendances.
Avec les droits SuperUser/root, vous pouvez utiliser `yum install` ou une interface graphique comme [Yum Extender](http://www.yumex.dk/) (s'il est installé).
[Sur Fedora version 22 et plus, yum est apparement mort](http://dnf.baseurl.org/2015/05/11/yum-is-dead-long-live-dnf/).

#### Microsoft Windows (et ReactOS?) et Apple OS X

Téléchargez le sur [le site web officiel](http://www.git-scm.com/downloads) et installez le.

### Utilisation

Si vous ne savez pas l'utiliser, vous devez lire une documentation (comme [l'officiel](http://www.git-scm.com/doc) ou [celle beaucoup plus courte d'OpenClassRooms](https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git)).

Utilisez `git status` pour vérifier quelles modifications vont être commitées.
Vous devriez utiliser `git pull` avant `git push`.

git fournit seulement une interface pour shell texte.
Cependant, vous pouvez installer une interface graphique pour git (comme [git-cola](https://git-cola.github.io/) ou [gitg](https://wiki.gnome.org/Apps/Gitg/)).
