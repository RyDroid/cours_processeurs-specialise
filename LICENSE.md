# Licences

Genérallement une licence est précisé au début d'un fichier.

## Fichiers sans licence explicite

- Si le fichier fait moins de 15 lignes, il est sous [la licence Creative Commons 0 v1.0](https://creativecommons.org/publicdomain/zero/1.0/) qui est équivalente au domaine public.
- Dans les autres cas, le fichier est sous :
  - [GPL](https://www.gnu.org/licenses/gpl.fr.html) version 2.0 et 3.0
  - [FDL](https://www.gnu.org/copyleft/fdl.fr.html) version 1.3
  - [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) version 4.0
  - [Licence Art Libre](http://artlibre.org/licence/lal/) version 1.3

## Plusieurs licences et plusieurs versions?

Vous pouvez choisir la licence qui vous convient le plus parmi s'il y en a plusieurs pour un fichier ou une licence compatible.
Il en est de même pour les numéros de version.

L'utilisation de plusieurs licences et plusieurs versions est faite pour maximiser la compatibilité juridique.
En effet, certaines licences sont incompatibles, c'est par exemple le cas de la GPL 2.0 et 3.0 avec la FDL 1.3.

## Contribution

Si vous contribuez à un fichier pré-existant, vous mettez votre contribution sous la même licence.
Si vous créez un nouveau fichier, vous devez préciser sa licence (qui doit être libre) ou accepter un licence libre implicite (voir la partie "Fichiers sans licence explicite").
