# Cours sur les processeurs spécialisés

## Contribuer

See [CONTRIBUTING.md](CONTRIBUTING.md).

## Licence(s) et auteurs

Lisez [LICENSE.md](LICENSE.md) et les logs de git pour une liste complète des contributeurs du projet.

## Passer des sources LaTeX à un PDF

Vous avez besoin de [LaTeX](https://fr.wikipedia.org/wiki/LaTeX).
Il est préférable d'avoir [xindy](http://xindy.org/), mais vous pouvez vous en passer en enlevant l'option xindy à l'inclusion du package glossaries dans "glossary.tex".

### Systèmes d'exploitation POSIX (GNU/Linux, *BSD, Apple OS X, etc)

Il est conseillé d'installé make pour vous simplifier la vie.
Si vous avez installé make, il vous suffit de l'utiliser avec la règle par défaut ou une autre.
Le résultat se trouve dans le dossier build.

#### Debian GNU/Linux et probablement ses dérivés (dont Trisquel et Ubuntu)

Les paquets [texlive-latex-recommended](https://packages.debian.org/stable/texlive-latex-recommended) et [texlive-lang-french](https://packages.debian.org/stable/texlive-lang-french) devraient suffir.
Néanmoins, il est recommandé d'avoir aussi [xindy](https://packages.debian.org/sid/xindy) et [make](https://packages.debian.org/stable/make).
Vous pouvez facilement les installer avec `apt-get install` ou `aptitude install` avec les droits root / SuperUser.

### Microsoft Windows (et ReactOS)

Il doit être possible d'obtenir d'utiliser ce projet sous Windows.
Si [Microsoft](https://www.gnu.org/proprietary/malware-microsoft.fr.html), [Windows](http://fr.windows7sins.org/) et d'une manière plus générale [les logiciels propriétaires / privateurs](https://www.gnu.org/proprietary/proprietary.fr.html) et [leurs principes contraires à l'éduction](https://www.gnu.org/education/education.fr.html) ne vous posent pas de problème, vous êtes invités à remplacer ce paragraphe par des explications.

## Foire aux Questions

- Pourquoi utilisez LaTeX ?
  C'est libre, gratuit et ça marche sur les principaux OS pour PCs de bureau (GNU/Linux, *BSD, Apple OS X et Microsoft Windows).
  De plus, c'est adapté au versionement avec un logiciel comme [git](https://fr.wikipedia.org/wiki/Git).
- Il y a des rectangles autour des liens, y a t'il une version imprimable ?
  LaTeX est intelligent : les rectangles ne seront pas imprimés.
  Si votre [lecteur PDF](https://pdfreaders.org/pdfreaders.fr.html) propose un aperçu avant impression, utilisez cette fonction avant de vous plaindre.
  Si vous trouvez que les liens sont plus visuellement dérangeants qu'utiles, vous pouvez imprimer le PDF dans un PDF (certains logiciels proposent l'option, sinon vous pouvez installer une fausse imprimante comme [CUPS-PDF](http://www.cups-pdf.de/download.shtml)).
- Une contribution sans utiliser git et/ou LaTeX sera t'elle acceptée ?
  À condition qu'elle soit pertinente, il n'y a aucune raison de ne pas l'intégrer.
  Néanmoins, cela demande un effort supplémentaire : en plus de devoir vérifier la pertinance, il faut également que l'intégrateur modifie les fichiers et potentiellement produise du code LaTeX lui-même.
  L'intégration sera donc plus longue.
  Une contribution par email avec des annotations sur un PDF est par exemple la bienvenue.
- Une contribution qui donne juste des idées et/ou mots-clés est elle utile ?
  Bien sur, des informations sans phrases est mieux que pas d'information du tout.
