# Copyright (C) 2015-2016  Nicola Spanti <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Folders
SRC_DIR=src
TEX_DIR=$(SRC_DIR)
IMG_DIR=$(SRC_DIR)/images
BUILD_DIR=build
PDF_DIR=$(BUILD_DIR)

# Commands
RM=rm -f
MKDIR=mkdir -p
LATEX2PDF=$(MKDIR) $(BUILD_DIR) && $(MKDIR) $(PDF_DIR) \
	&& cd $(TEX_DIR) \
	&& pdflatex -output-format pdf -output-directory ../$(PDF_DIR)

# Package and archive
PACKAGE=cours-processeurs-specialise
SRC_FILES_TO_ARCHIVE=$(SRC_DIR)/*.tex $(IMG_DIR)/* licenses/* makefile *.md \
	.gitignore .editorconfig .gitlab-ci.yml


all: cours presentation


ouvrir-cours: cours
	xdg-open $(PDF_DIR)/cours.pdf &

ouvrir-presentation: presentation
	xdg-open $(PDF_DIR)/presentation.pdf &


dist: default-archive

default-archive: zip

zip: zip-pdf zip-src

zip-pdf: fr clean-tmp
	@$(MKDIR) $(BUILD_DIR)
	zip $(BUILD_DIR)/pdf.zip -- $(PDF_DIR)/*.pdf

zip-src: clean
	@$(MKDIR) $(BUILD_DIR)
	zip $(BUILD_DIR)/src.zip -r -- $(SRC_FILES_TO_ARCHIVE)


fr: cours presentation

cours: $(PDF_DIR)/cours.pdf

$(PDF_DIR)/cours.pdf: \
		$(TEX_DIR)/cours*.tex $(PDF_DIR)/cours.gls \
		$(TEX_DIR)/preamble.tex $(IMG_DIR)/*
	$(LATEX2PDF) cours.tex
	$(LATEX2PDF) cours.tex

$(PDF_DIR)/cours.gls: $(TEX_DIR)/glossary.tex
	$(LATEX2PDF) cours.tex
	cd $(PDF_DIR) && makeglossaries cours.glo

presentation: $(PDF_DIR)/presentation.pdf

$(PDF_DIR)/presentation.pdf: \
		$(TEX_DIR)/presentation.tex \
		$(TEX_DIR)/preamble.tex $(IMG_DIR)/*
	$(LATEX2PDF) presentation.tex
	$(LATEX2PDF) presentation.tex


clean: \
		clean-tmp clean-bin clean-python clean-profiling clean-ide \
		clean-archives clean-build
	cd $(SRC_DIR) && make --directory=.. clean-tmp clean-latex

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* log/ logs/ \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-bin:
	$(RM) -f -- *.o *.a *.so *.ko *.lo *.dll *.out

clean-python:
	$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	$(RM) -f -- callgrind.out.*

clean-ide:
	$(RM) -f -- qmake_makefile *.pro.user *.cbp *.CBP

clean-archives:
	$(RM) -f -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav *.out \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.xz \
		*.synctex.gz *.synctex.gzip \
		*.synctex.bz2 *.synctex.bzip \
		*.synctex.lz *.synctex.lzma \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-build:
	$(RM) -rf -- build/ Build/ BUILD/

clean-git:
	git clean -fdx
