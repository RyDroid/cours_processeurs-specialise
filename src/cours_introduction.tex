\section{Introduction}

\subsection{Hétérogénéité des architectures de processeurs}

\begin{tabular}{|l|l|l|l|}
  \hline
  Nom      & Type       & Logique      & Coût et performance \\
  \hline
  GPP      & Général    & Intégrée     & Élevés              \\
  AP / SoC & Général    & Intégrée     & Moyens              \\
  MCU      & Général    & Intégrée     & Faibles             \\
  DSP      & Spécialisé & Intégrée     & Faibles-Moyens      \\
  GPU      & Spécialisé & Intégrée     & Élevés              \\
  FPGA     & Spécialisé & Programmable & Faibles             \\
  SoC      & Spécialisé & Programmable & Moyens              \\
  "ASIC"   & Spécialisé & Spécialisée  & Très élevés         \\
  \hline
\end{tabular}

Processeur application et co-processeur sont parfois respectivement vus comme des synonymes de processeur général et processeur spécialisé.
Néanmoins, il faut faire attention car dans de très rares cas il se peut qu'un processeur spécialisé soit utilisé comme processeur application ou qu'un processeur général soit utilisé comme un co-processeur.

Les processeurs à logique intégrée ont au moins un \gls{CPU_abbr} et on utilise des langages de programmation pour les manipuler.
Parmi les langages de programmation utilisés, on peut citer le C, le C++, \href{https://fr.wikipedia.org/wiki/Java_\%28langage\%29}{le Java} (notamment avec \href{https://fr.wikipedia.org/wiki/Java_Card}{Java Card} pour les cartes à puce) et \href{https://www.rust-lang.org/}{le Rust}.
Les processeurs à logique programmable n'ont pas de CPU et on les utilise via des langages de description (comme VHDL et Verilog).

\subsubsection{\acrfull*{GPP_abbr}}

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Polyvalent
  \item Évolutif
  \item \gls{proc-superscalaire}
  \item Performance
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Coût (20€ à plusieurs milliers d'euros)
  \item Encombrement
  \item Échauffement
  \end{itemize}
\end{itemize}

Un GPP contient un ou plusieurs \gls{CPU_abbr} et un ou plusieurs \href{https://en.wikipedia.org/wiki/CPU_cache}{caches}.
Un GPP s'utilise avec une carte mère (System on Board).
Pour faire fonctionner le GPP sur une carte mère, il faut qu'elle soit équipée de mémoire vive (\gls{RAM_abbr}) et d'un périphérique de stockage persistant (comme un \gls{HDD_abbr} ou un \gls{SSD_abbr}).

Parmi les acteurs importants on peut citer : \href{https://fr.wikipedia.org/wiki/Intel}{Intel}, \href{https://fr.wikipedia.org/wiki/Advanced_Micro_Devices}{AMD} et \href{https://fr.wikipedia.org/wiki/Alliance_AIM}{l'alliance AIM} (avec \href{https://fr.wikipedia.org/wiki/PowerPC}{l'architecture PowerPC}).

\subsubsection{\acrfull*{AP_abbr} / System on Chip}

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Polyvalent
  \item \gls{proc-superscalaire}
  \item Performance (presque comme un GPP pour un haut de gamme)
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Pas évolutif
  \item Coût (5€ à 60€)
  \end{itemize}
\end{itemize}

Parmi les acteurs importants on peut citer
\href{https://fr.wikipedia.org/wiki/ARM_\%28soci\%C3\%A9t\%C3\%A9\%29}{ARM} et
\href{https://fr.wikipedia.org/wiki/MIPS_Technologies}{MIPS Technologies}.

\subsubsection{\acrfull*{MCU_abbr}}

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Coût (entre 0,05€ à 5€)
  \item Polyvalent
  \item Mémoire vive et persistante intégrée, ainsi que des ports \gls{IO_abbr}
  \item Échauffement
  \item Encombrement
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Pas évolutif
  \item Performance
  \item Pas d'OS
  \end{itemize}
\end{itemize}

Parmi les acteurs importants on peut citer :
\href{https://fr.wikipedia.org/wiki/Renesas_Electronics_Corporation}{Renesas},
\href{https://fr.wikipedia.org/wiki/STMicroelectronics}{STMicroelectronics},
\href{https://fr.wikipedia.org/wiki/Texas_Instruments}{Texas Instruments} et
\href{https://fr.wikipedia.org/wiki/Microchip_Technology}{Microchip}.

\subsubsection{\acrfull*{GPU_abbr}}

Attention : Un GPU ne doit pas être confondu avec une carte graphique.
En effet, une carte graphique est composée d'un GPU, de \gls{RAM_abbr} et potentiellement de périphériques (comme une sortie HDMI ou VGA).
Les périphériques ne sont par exemple pas utiles dans certains cas de \gls{GPGPU_abbr} notamment quand cette technique est utilisée pour un super-calculateur.

Un \gls{GPU_abbr} est très souvent composé de plusieurs \gls{CPU_abbr} individuellement plus simples qu'un \gls{MCU_abbr}.
Ces CPU peuvent se compter en centaines voire en milliers.
Un GPU est donc massivement parallèle.
Cela nécessite beaucoup de transistors, notamment à cause de caches (rempli via des \gls{LdSt_abbr}) pour des grappes de CPU, c'est pourquoi un GPU est un composant au fort échauffement.
Les caches sont utiles pour accéder rapidement à la mémoire, mais avant que les données soient dedans il faut qu'elles soient dans la RAM de la carte graphique qui les obtient grâce à la RAM principale, copier beaucoup de données pour les traiter avec un GPU est donc un goulot d'étranglement.

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Performance via le parallélisme
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Coût (cher)
  \item Accès mémoire principal
  \item Échauffement
  \item Pas évolutif
  \item Pas d'OS
  \end{itemize}
\end{itemize}

Parmi les acteurs importants on peut citer :
\href{https://fr.wikipedia.org/wiki/Intel}{Intel} (avec ses \gls{IGP_abbr}),
\href{https://fr.wikipedia.org/wiki/Nvidia}{Nvidia} et
\href{https://fr.wikipedia.org/wiki/ATI_Technologies}{ATI} (qui appartient à AMD).

\subsubsection{\acrfull*{DSP_abbr}}

Attention : Un DSP n'a pas forcément plusieurs cœurs.

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Coût (généralement entre 1€ à 10€, mais jusqu'à 60€ pour le très haut de gamme)
  \item Mémoire vive et persistante intégrée, ainsi que des ports \gls{IO_abbr}
  \item Échauffement
  \item Encombrement
  \item Performance (mais inférieure à un \gls{AP_abbr})
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Pas évolutif
  \item Pas d'OS
  \end{itemize}
\end{itemize}

Les applications majeures sont le traitement de signal 1D et 2D (comme de l'audio ou une image 2D).
On retrouve donc principalement les DSP, dans le domaine militaire, les radars, les télécoms et la médecine.
Parmi les acteurs importants on peut citer :
\href{https://fr.wikipedia.org/wiki/Texas_Instruments}{Texas Instruments},
\href{https://fr.wikipedia.org/wiki/Analog_Devices}{Analog Devices} et
\href{https://fr.wikipedia.org/wiki/Freescale_Semiconductor}{Freescale Semiconductor}.

\subsubsection{\acrfull*{FPGA_abbr}}

Un FPGA est une architecture vierge.
Il faut donc la programmer matériellement, un FPGA est donc customisable, on peut donc parler de « soft core ».
Cela permet de coder matériellement les fonctions voulues et dans la quantité voulue.
Malheuresement, la programmation matérielle peut durer plusieurs heures.
En 2015, il n'y a pas de programmation « à chaud », mais cela pourrait arriver quelques années plus tard.

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Coût (plus qu'un MCU, à peu près comme un DSP, et moins qu'un GPP ou GPU)
  \item Customisable
  \item Performance
  \item Parallélisme
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Compétences humaines nécessaires (\href{https://fr.wikipedia.org/wiki/Verilog}{Verilog} ou \href{https://fr.wikipedia.org/wiki/VHDL}{VHDL}) rares en 2015
  \item Logiciels en général moins conviviaux et puissants que pour d'autres types d'architectures en 2015
  \item Programmation matérielle lente
  \end{itemize}
\end{itemize}

\subsubsection{\acrfull*{SoC_abbr}}

Un SoC est composé d'un AP, de périphériques et éventuellement de un ou plusieurs DSP et FPGA.
Le coût d'un SoC est similaire à celui d'un AP.
De plus, un SoC a les mêmes avantages et inconvéniants qu'un AP.

\subsubsection{\acrfull*{ASIC_abbr}}

Un ASIC est spécifique à une application, et a donc des fonctionnalités uniques ou sur mesure pour celle-ci.

\begin{itemize}
\item Avantages
  \begin{itemize}
  \item Performance
  \item Échauffement
  \item Encombrement
  \item Coût de production après conception (entre 0,1€ à 10€)
  \end{itemize}
\item Inconvénients
  \begin{itemize}
  \item Coût de conception très élevé
  \item Pas évolutif
  \item Pas polyvalent
  \end{itemize}
\end{itemize}

\subsection{Choisir une architecture de processeur}

Pour un problème, il peut y avoir plusieurs solutions.
Cela peut être le cas avec les architectures de processeurs.

On peut par exemple faire à peu près tout avec un GPP ou un AP.
Néanmoins, un DSP qui est beaucoup moins couteux peut dans certains cas faire un même traitement au moins aussi rapidement tout en étant moins encombrant et moins consommateur d'énergie.
Un DSP n'est pourtant pas une solution parfaite, parce qu'il faut faire du développement spécifique pour un DSP pour tirer pleinement parti de sa puissance, tandis qu'un développement sur AP ou GPP est générique.
Dévelloper sur DSP est donc plus long, plus difficile et donc plus couteux, et cela est accentué par la recherche de profils spécialisés ou de temps pour spécialiser des programmeurs.

Une dépendance envers le créateur d'une architecture peut être un problème.
Celui-ci peut disparaitre ou arrêter d'améliorer une de ses gammes de processeurs.
Cela peut être particulièrement un problème quand du développement spécifique a été fait pour une architecture de processeur.
Il faut donc privilégier les architectures pour lesquelles il y a \href{https://www.gnu.org/philosophy/free-hardware-designs.fr.html}{des plans libres}.

\subsection{Toolchain}

Pour compiler du code source, une chaine de compilation est nécessaire.
En 2015 et depuis de nombreuses années, le langage dominant dans l'embarqué est le C.
On essaye généralement d'éviter l'assembleur à cause de sa complexité et son abscence de portabilité.
Le C++ est utilisé généralement quand on veut faire une interface graphique.

La \gls{toolchain} \gls{GNU} est libre, gratuite, performante et marche sur de nombreux systèmes d'exploitation.
Ses principaux éléments sont :
\begin{itemize}
\item \href{https://www.gnu.org/software/binutils/}{GNU Binutils}
  \begin{itemize}
  \item as : l'assembleur GNU
  \item ld : le linker GNU
  \item D'autres programmes
  \end{itemize}
\item \gls{GCC_abbr} (avec la commande gcc pour le C et g++ pour le C++)
\item \href{https://www.gnu.org/software/libc/}{glibc} : la librairie C
\item libstdc++ : la librairie C++
\item \gls{GDB_abbr}
\item \href{https://www.gnu.org/software/emacs/}{GNU Emacs} : bien plus qu'un éditeur de texte
\end{itemize}

\subsection{Système d'exploitation}

Pour l'embarqué, un \gls{OS_abbr} est parfois utile car il offre des services.
Un \gls{MCU_abbr} n'est pas suffisant, c'est pourquoi on utilise un \gls{AP_abbr}.

Parmi les OS connus au moins en grande partie libres, on peut citer :
\begin{itemize}
\item Léger sans serveur graphique obligatoire et plus ou moins lourd
  \begin{itemize}
  \item \gls{GNU} / \gls{Linux} (comme \href{https://www.debian.org/index.fr.html}{Debian} GNU/Linux)
  \item Les dérivés de \gls{BSD_abbr} (comme \href{https://www.freebsd.org/}{FreeBSD} et \href{https://www.netbsd.org/about/embed.html}{NetBSD})
  \end{itemize}
\item \href{https://source.android.com/}{Android} (et ses dérivés comme \href{http://www.replicant.us/}{Replicant}) (qui est basé sur le noyau Linux, mais sans GNU) : lourd mais avec un serveur graphique et une grande \gls{API_abbr}
\end{itemize}

\subsection{Chargeur d'amorçage}

L'initialisation du matériel est généralement faite de manière matérielle au moins pour les AP / SoC et SoB + GPP en 2015.
Pour cela on utilise un chargeur d'amorçage appelé bootloader en anglais.

Pour l'embarqué, il existe \href{https://fr.wikipedia.org/wiki/Das_U-Boot}{U-Boot} qui est libre.
Pour des AP et des SoB + GPP, il y a \href{https://en.wikipedia.org/wiki/Coreboot}{Coreboot} et un dérivé entièrement libre qui se nomme \href{http://www.libreboot.org/}{Libreboot}.

\subsection{Rappels sur les types de données}

\subsubsection{Opérations sur des types entiers}

Les types utilisés sont ceux de C99 ou ont des noms similaires (il n'y a pas de types avec un nombre impair de bits en C99 et C11).

\begin{itemize}
  \item uint16\_t + uint16\_t = uint17\_t ($2^{16} + 2^{16} = 2^{17}$)
  \item uint16\_t $\times$ uint16\_t = uint32\_t ($2^{16} \times 2^{16} = 2^{32}$)
  \item int16\_t $\times$ int16\_t = int31\_t ($2^{15} \times 2^{15} = 2^{30}$, mais il faut rajouter le bit de signe)
\end{itemize}

\subsubsection{Types importants en langage C}

\begin{tabular}{|l|l|}
  \hline
  Type              & Explication                                           \\
  \hline
  char              & Entier représentant un caractère au moins 8 bits      \\
  signed char       & Comme char, mais est forcément signé                  \\
  unsigned char     & Comme char, mais est forcément non signé              \\
  short             & Entier signé d'au moins 16 bits                       \\
  short int         & Comme short                                           \\
  signed short      & Comme short                                           \\
  signed short  int & Comme short                                           \\
  int               & Entier d'au moins 16 bits                             \\
  signed            & Comme int                                             \\
  signed int        & Comme int                                             \\
  unsigned          & Comme int, mais signé                                 \\
  unsigned int      & Comme unsigned                                        \\
  long              & Entier signé d'au moins 32 bits                       \\
  long int          & Comme long                                            \\
  signed long       & Comme long                                            \\
  signed long int   & Comme long                                            \\
  \hline
  float             & Nombre flottant (généralement simple précision)       \\
  double            & Nombre flottant (généralement à double précision)     \\
  \hline
  bool (C99)        & Type booléen (true ou false) (inclure stdbool.h)      \\
  \hline
\end{tabular}

Pour un type, l'étoile "*" indique que c'est un pointeur sur le type précédé par "*".
L'étoile "*" est l'opérateur de multiplication, mais aussi de \href{https://fr.wikibooks.org/wiki/Programmation_C-C\%2B\%2B/D\%C3\%A9r\%C3\%A9f\%C3\%A9rencement,_indirection}{déréférencement}.
On peut faire un pointeur sur une fonction.

Pour plus de détails, \href{https://en.wikipedia.org/wiki/C_data_types#cite_note-c99sizes-3}{l'article \foreign{C data types} de Wikipedia (en)} est exhaustif tout en donnant des références.
